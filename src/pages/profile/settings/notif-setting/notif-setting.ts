import { Component } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../../providers/api-service/api-service';
import { NotifModalPage } from './notif-modal';

@IonicPage()
@Component({
  selector: 'page-notif-setting',
  templateUrl: 'notif-setting.html',
})
export class NotifSettingPage {
  islogin: any;
  dataOriginal: any;
  fData: any = {};
  fDataa: any = {};
  notifType: any;
  isAddEmail: boolean = false;
  emailList: any;
  isAddPhone: boolean = false;
  phonelist: any;
  shownotifdiv: boolean = true;
  newArray: any = [];
  lowbattery: boolean = true
  lowbatteryy: boolean = true
  lowbatteryyy: boolean = true
  lt: any;
  dataa: any = {
    // ign: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // poi: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // power: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // fuel: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // geo: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // overspeed: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // AC: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // route: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // maxstop: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // sos: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // sms: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    //  vibration: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
     lowBattery: {
      sms_status: false, email_status: false,
      notif_status: false,
      priority: 1, emails:[], phones:[]
    }
    // accAlarm: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // toll: {
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // harshBreak:{
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },
    // harshAcceleration:{
    //   sms_status: false, email_status: false,
    //   notif_status: false,
    //   priority: 1, emails:[], phones:[]
    // },

  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    private toastCtrl: ToastController,
    private modalCtrl: ModalController,
    public platform: Platform,
    // private nativeAudio: NativeAudio
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.getCustDetails();

    if (localStorage.getItem('DisplayimmombilizeMode') != null) {
      if (localStorage.getItem('DisplayimmombilizeMode') === 'OFF') {
        this.lowbattery = false;
      }
    }
    if (localStorage.getItem('DisplayimmombilizeMode') != null) {
      if (localStorage.getItem('DisplayimmombilizeMode') === 'OFF') {
        this.lowbatteryy = false;
      }
    }
    if (localStorage.getItem('DisplayimmombilizeMode') != null) {
      if (localStorage.getItem('DisplayimmombilizeMode') === 'OFF') {
        this.lowbatteryyy = false;
      }
    }
    // console.log("check here: ", this.notifArray);
    // this.sortArray();


    // The Native Audio plugin can only be called once the platform is ready
    // this.platform.ready().then(() => {
    //   console.log("platform ready");

    //   // This is used to unload the track. It's useful if you're experimenting with track locations
    //   this.nativeAudio.unload('trackID').then(function () {
    //     console.log("unloaded audio!");
    //   }, function (err) {
    //     console.log("couldn't unload audio... " + err);
    //   });

    //   // 'trackID' can be anything
    //   this.nativeAudio.preloadComplex('trackID', 'assets/audio/test.mp3', 1, 1, 0).then(function () {
    //     console.log("audio loaded!");
    //   }, function (err) {
    //     console.log("audio failed: " + err);
    //   });
    // });

  }

  getCustDetails() {
    var _bUrl = this.apiCall.mainUrl + 'users/getCustumerDetail?uid=' + this.islogin._id;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        // debugger
        console.log("resp data: ", respData);
        console.log("p2", respData.cust.pass);
        this.lt = respData.cust.pass;
        localStorage.setItem('pwd2', this.lt);
        if (respData.length === 0) {
          return;
        } else {
          if (respData.cust.alert) {
            var result = Object.keys(respData.cust.alert).map(function (key) {
              return [key, respData.cust.alert[key]];
            });
            this.newArray = result.map(function (r) {
              return {
                key: r[0],
                imgUrl: 'assets/notificationIcon/' + r[0] + '.png',
                keys: r[1]
              }
            });

            console.log("someArr: " + this.newArray)
          }
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter NotifSettingPage');
  }

  onChange(ev) {
    console.log("event: ", ev)
  }

  tabb(key1, key2) {
  if (key1 && key2)
    this.dataa[key1][key2] = !this.dataa[key1][key2];

  if (JSON.stringify(this.dataa) === JSON.stringify(this.dataOriginal))
    return;

  this.fDataa.contactid = this.islogin._id;
  this.fDataa.alert = this.dataa;
  //this.Load = true;
  var url = this.apiCall.mainUrl + 'users/editUserDetails';
  this.apiCall.startLoading().present();
  this.apiCall.urlpasseswithdata(url, this.fData)
    .subscribe(respData => {
      this.apiCall.stopLoading();
      console.log("check stat: ", respData);
      if (respData) {
        this.toastCtrl.create({
          message: 'Setting Updated',
          duration: 1500,
          position: 'bottom'
        }).present();
      }
    },
      err => {
        this.apiCall.stopLoading();
      });
}

  tab(key1, key2) {
    debugger
    if (key1 && key2) {
      if (key1.keys[key2] === false) {
        key1.keys[key2] = false;
      } else {
        key1.keys[key2] = true;
      }
    }
    for (var t = 0; t < this.newArray.length; t++) {
      if (this.newArray[t].key === key1.key) {
        this.newArray[t].keys = key1.keys;
      }
    }
    var temp = [];
    for (var e = 0; e < this.newArray.length; e++) {
      temp.push({
        [this.newArray[e].key]: this.newArray[e].keys,
        key: this.newArray[e].key
      });
    }
    /* covert an array into object */
    var res = {};
    temp.forEach(function (a) { res[a.key] = a[a.key] })

    this.fData.contactid = this.islogin._id;
    this.fData.alert = res;
    var url = this.apiCall.mainUrl + 'users/editUserDetails';
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(url, this.fData)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log("check stat: ", respData);
        if (respData) {
          this.toastCtrl.create({
            message: 'Setting Updated',
            duration: 1500,
            position: 'bottom'
          }).present();
        }
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  addEmail(noti) {
    // use AudioProvider to control selected track

    this.notifType = noti.key;
    this.isAddEmail = true;
    var data = {
      "buttonClick": 'email',
      "notifType": this.notifType,
      "compData": {
        [this.notifType]: noti.keys
      },
      "newArray":this.newArray
    }

    const modal = this.modalCtrl.create(NotifModalPage, {
      "notifData": data
    });
    modal.present();
  }

  addPhone(noti) {
    this.notifType = noti.key;
    this.isAddPhone = true;
    // var data = {
    //   buttonClick: 'phone',
    //   "notifType": this.notifType,
    //   "compData": { [this.notifType]: noti.keys }
    // }

    var data = {
      "buttonClick": 'phone',
      "notifType": this.notifType,
      "compData": {
        [this.notifType]: noti.keys
      },
      "newArray":this.newArray
    }

    const modal = this.modalCtrl.create(NotifModalPage, {
      "notifData": data
    });
    modal.present();
  }

  // openTimePicker(data) {
  //   const modal = this.modalCtrl.create(TimePickerModal, {
  //     data: data
  //   });
  //   modal.present();
  // }

}
