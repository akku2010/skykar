import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, ModalController, ViewController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';
import { DaywiseDistancePage } from '../daywise-distance/daywise-distance';

@IonicPage()
@Component({
  selector: 'page-distance-report',
  templateUrl: 'distance-report.html',
})
export class DistanceReportPage implements OnInit {

  locationAddress: any;
  locationEndAddress: any;
  distanceReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices: any;
  portstemp: any;
  datetime: number;
  selectedVehicle: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  pltStr: string;

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallDistance: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider,
    private plt: Platform,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController
  ) {
    if (this.plt.is('android')) {
      this.pltStr = 'md';
    } else if (this.plt.is('ios')) {
      this.pltStr = 'ios';
    }
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    // this.datetimeStart = moment({ hours: 0 }).format();
    // this.datetimeEnd = moment().format();//new Date(a).toISOString();

    this.datetimeStart = moment({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
    console.log("today time: ", this.datetimeStart)
    this.datetimeEnd = moment({ hours: 0 }).format(); // today date and time with 12:00am
  }

  ngOnInit() {
    this.getdevices();
  }

  ionViewDidEnter() {
    this.getDefaultUserSettings();
  }
  measurementUnit: string = 'MKS';
  getDefaultUserSettings() {
    var b_url = this.apicallDistance.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apicallDistance.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }

  getdevices() {
    var baseURLp = this.apicallDistance.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicallDistance.startLoading().present();
    this.apicallDistance.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicallDistance.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicallDistance.stopLoading();
          console.log(err);
        });
  }

  getdistancedevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle._id;
  }

  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.distanceReportData);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "ignition_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  daywisedistance(d) {
    let pModal = this.modalCtrl.create(DaywiseDistancePage, {
      param: d,
      'fdate': this.datetimeStart,
      'tdate': this.datetimeEnd,
      'uid': this.islogin._id,
      'selectedVehicle': this.Ignitiondevice_id
    });
    pModal.onDidDismiss(() => {
      // this.viewCtrl.dismiss();
      // this.getdevices();
    })
    pModal.present();
  }

  distanceReportData = [];
  getDistanceReport() {
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";
    }
    let outerthis = this;
    this.apicallDistance.startLoading().present();
    this.apicallDistance.getDistanceReportApi(new Date(outerthis.datetimeStart).toISOString(), new Date(outerthis.datetimeEnd).toISOString(), this.islogin._id, this.Ignitiondevice_id)
      .subscribe(data => {
        this.apicallDistance.stopLoading();
        this.distanceReport = data;
        if (this.distanceReport.length > 0) {
          this.innerFunc(this.distanceReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicallDistance.stopLoading();
        console.log(error);
      })
  }

  innerFunc(distanceReport) {
    let outerthis = this;
    var i = 0, howManyTimes = distanceReport.length;
    function f() {

      outerthis.distanceReportData.push({
        'distance': distanceReport[i].distance,
        'Device_Name': distanceReport[i].device.Device_Name,
        'end_location': {
          'lat': distanceReport[i].endLat,
          'long': distanceReport[i].endLng
        },
        'start_location': {
          'lat': distanceReport[i].startLat,
          'long': distanceReport[i].startLng
        }
      });

      outerthis.start_address(distanceReport[i], i);
      outerthis.end_address(distanceReport[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  // start_address(item, index) {
  //   let that = this;
  //   that.distanceReportData[index].StartLocation = "N/A";
  //   if (item.start_location == null || item.start_location == undefined) {
  //     that.distanceReportData[index].StartLocation = "N/A";
  //   } else if (item.start_location != null || item.start_location != undefined) {
  //     this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
  //       .then((res) => {
  //         console.log("test", res)
  //         var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //         that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
  //         that.distanceReportData[index].StartLocation = str;
  //       })
  //   }
  // }

  start_address(item, index) {
    debugger
    let that = this;
    if (!item.start_location) {
      that.distanceReportData[index].StartLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.apicallDistance.getAddress(tempcord)
      .subscribe(res => {
        console.log("test");
        console.log("result", res);
        console.log(res.address);
        // that.allDevices[index].address = res.address;
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
              that.distanceReportData[index].StartLocation = str;
              // console.log("inside", that.address);
            })
        } else {
          that.distanceReportData[index].StartLocation = res.address;
        }
      })
      console.log("s", this.distanceReportData);
  }

  // end_address(item, index) {
  //   let that = this;
  //   that.distanceReportData[index].EndLocation = "N/A";
  //   if (item.end_location == null || item.end_location == undefined) {
  //     that.distanceReportData[index].EndLocation = "N/A";
  //   } else if (item.end_location != null || item.end_location != undefined) {
  //     this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
  //       .then((res) => {
  //         var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //         that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
  //         that.distanceReportData[index].EndLocation = str;
  //       })
  //   }
  // }

  end_address(item, index) {
    let that = this;
    if (!item.end_location) {
      that.distanceReportData[index].EndLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.end_location.lat,
      "long": item.end_location.long
    }
    this.apicallDistance.getAddress(tempcord)
      .subscribe(res => {
        console.log("test");
        console.log("endlocation result", res);
        console.log(res.address);
        // that.allDevices[index].address = res.address;
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.end_location.lat, item.end_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
              that.distanceReportData[index].EndLocation = str;
              // console.log("inside", that.address);
            })
        } else {
          console.log("enter in else")
          that.distanceReportData[index].EndLocation = res.address;
        }
      })
      console.log("e", this.distanceReportData);
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicallDistance.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

}
