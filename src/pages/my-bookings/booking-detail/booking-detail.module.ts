import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookingDetailPage } from './booking-detail';
import { SignaturePadModule } from 'angular2-signaturepad';
@NgModule({
  declarations: [
    BookingDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BookingDetailPage),
    SignaturePadModule
  ],
})
export class BookingDetailPageModule {}
